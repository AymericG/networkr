var networkerViewModel = new NetworkerModel();

$(function () {
    ko.applyBindings(networkerViewModel, document.getElementById("networkerWrap"));
    var list = document.getElementById("listt");
    list.addEventListener("delete", function (evt) {
        networkerViewModel.removeElement($(evt.target).find('._idElm').text());
    });
    new SwipeOut(list);
})