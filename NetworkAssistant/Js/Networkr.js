function NetworkerModel() {
    var self = this;

    //#region Templates

    var contact = function (name, note, counter, id,startDate) {
        this.Id = isUndefinedValue(id) ? newGuid() : id;
        this.Name = ko.observable(name);
        this.Note = ko.observable(note);
        this.Counter = ko.observable(counter);
        this.defaultCounter = counter;
        this.startDate = isUndefinedValue(startDate) ? new Date() : startDate;
    };

    var timePeriod = function (name, value) {
        this.Name = name;
        this.Value = value;
    };

    //#endregion

    //#region Fields...

    self.ContactsList = ko.observableArray([]);

    var editItem = null;
    self.editName = ko.observable('');
    self.editNote = ko.observable('');
    self.titleText = ko.observable('Networkr');
    self.isValidationError = ko.observable(false);
    self.isEditPage = ko.observable(false);
    self.selectedPeriod = ko.observable('');
    
    self.timePeriods = ko.observableArray([
        new timePeriod("weekly", 7),
        new timePeriod("fortnightly", 14),
        new timePeriod("monthly", 30),
        new timePeriod("half yearly", 180),
        new timePeriod("yearly", 365)
    ]);

    self.editName.subscribe(function () {
        if (isEditFieldsValid()) {
            self.isValidationError(false);
        } else {
            self.isValidationError(true);
        }
    });

    //#endregion

    //#region Constructor

    function initModel() {
        if (isUndefinedValue(amplify.store("contactsList"))) {
            initDefault();
        } else {
            initDataFromStorage();
        }
    }

    initModel();

    //#endregion

    //#region Public Methods...

    self.addNewContact = function () {
        openEditPage();
        editItem = null;
        changeTitle('Contact');
    };

    self.editElement = function (data) {
        openEditPage();
        editItem = data;
        self.editName(data.Name());
        self.editNote(data.Note());
        self.selectedPeriod(getTimePeriod(data.defaultCounter));
        changeTitle('Contact');
    };

    self.saveChanges = function () {
        if (isEditFieldsValid()) {
            if (null != editItem) {
                editItem.Name(self.editName());
                editItem.Note(self.editNote());
                editItem.Counter(self.selectedPeriod().Value);
                editItem.defaultCounter = self.selectedPeriod().Value;
                editItem.startDate =new Date();
            } else {
                self.ContactsList.push(
                    new contact(self.editName(),
                        self.editNote(),
                        self.selectedPeriod().Value));
            }
            reorderElements();
            storeLocalData();
            clearEditValues();
        } else {
            self.isValidationError(true);
        }
    };

    self.back = function () {
        clearEditValues();
    };

    self.resetCounter = function (data, event) {
        data.Counter(data.defaultCounter);
        data.startDate = new Date();
        event.stopImmediatePropagation();
        reorderElements();
        storeLocalData();
    };

    self.removeElement = function (id) {
        if (!isUndefinedValue(id)) {
            $.each(self.ContactsList(), function (key, value) {
                if (value.Id == id) {
                    self.ContactsList.remove(value);
                    storeLocalData();
                }
            });
        }
    };

    //#endregion

    //#region Private Methods...

    function clearEditValues() {
        closeEditPage();
        self.editName('');
        self.editNote('');
        editItem = null;
        changeTitle('Networkr');
        self.isValidationError(false);
        self.selectedPeriod(self.timePeriods()[0]);
    }

    function reorderElements() {
        self.ContactsList.sort(function (left, right) {
            var leftCounter = parseInt(left.Counter());
            var rightCounter = parseInt(right.Counter());
            return leftCounter == rightCounter ?
                        left.Name().localeCompare(right.Name()) :
                            (leftCounter < rightCounter ? -1 : 1);
        });
    }

    function changeTitle(text) {
        self.titleText(text);
    }

    function isEditFieldsValid() {
        return $.trim(self.editName()).length != 0;
    }

    function openEditPage() {
        $("body").addClass("edit-mode");
        self.isEditPage(true);
    }

    function closeEditPage() {
        $("body").removeClass("edit-mode");
        self.isEditPage(false);
    }

    function initDefault() {
        //self.ContactsList.push(new contact("Anthony", '', -5));
        //self.ContactsList.push(new contact("Robert", 'McDonalds manager', -1));
        //self.ContactsList.push(new contact("John Smith", 'Starbucks', 5));
        //self.ContactsList.push(new contact("Daddy", '', 10));

        storeLocalData();
    }

    function storeLocalData() {
        amplify.store("contactsList", null);
        amplify.store("contactsList", ko.toJS(self.ContactsList()));
    }

    function initDataFromStorage() {
        var currentDate = new Date();
        $.each(amplify.store("contactsList"), function (key, value) {
            var contactElm = new contact(value.Name, value.Note, value.Counter, value.Id, value.startDate);
            var elmDate = new Date(contactElm.startDate);
            if (elmDate < currentDate) {
                var timeDiff = Math.abs(currentDate.getTime() - elmDate.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1;
                contactElm.Counter(contactElm.Counter() - diffDays);
            }
            
            self.ContactsList.push(contactElm);
        });
    }

    function newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    function isUndefinedValue(value) {
        return 'undefined' === typeof value; // even Work On IPad and IE7
    }

    function getTimePeriod(number) {
        var timeLen = self.timePeriods().length;
        var res = null;
        for (var i = 0; i < timeLen; i++) {
            var period = self.timePeriods()[i];
            if (parseInt(period.Value) == parseInt(number)) {
                res = period;
                break;
            }
        }
        return res;
    }
    
    //#endregion
}